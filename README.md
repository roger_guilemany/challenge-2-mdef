# Challenge 2 mdef

Project by:

<a href="https://krzysztof_wronski.gitlab.io/wrona/">Krzysztof Wronski</a><br>
<a href="https://roger_guilemany.gitlab.io/mdef-website/">Roger Guilemany</a>

This is the documentation for the second challenge from fabacademy (mdef). Temporary name The Most Complex Box.

Building upon the idea of enacting non-human actors in our design practice, and from the discussion on how to interpret their voice, we wanted to develop a speculative artifact in that direction. A lot of possibilities have emerged as the spectrum is very broad.

<img src="images/introduction/parliament-of-plants_studio-celine -baumann.png">

Here you can see some of the ideas we have been shuffling around to define the concept.

<img src="images/introduction/ideation.png">

The idea has been formalised as a speaker that can be attached to these actors in order to create a situation where humans dedicate time listening to these non-human voices. The human designer becomes the speaking subject of the system. The device is pure speculation with the idea behind to reflect on the time and attention we dedicate to other species and things in our practice as human designers.

<img src="images/introduction/sketch.png">

## References

There are a lot of projects in that direction. We will share some that have inspired us in the development of this artifact.

**THE GREEN OBSERVATORY**
*Antoine Jaunard*

The Green Observatorynis a device that listens to the electrical activity of plants and represents it with a physical and meditative movement that we, humans, can observe and contemplate.

http://fabacademy.org/2020/labs/barcelona/students/antoine-jaunard/green-observatory.html

<img src="images/references/green-observatory.png">

**CONCERTO PARA PLANTAS**
*Jose Venditti & Gabriel Alonso*

Concerto for plants is an experimental project between the visual arts and the sound arts in which the listener is invited to rethink our relationship with nature, establishing an active dialogue between humans and plants. The proposal tries to place the vegetal world at the center of the artistic experience, to abandon the anthropocentric vision of the world and make a new field of aesthetic research possible.

https://instituteforpostnaturalstudies.org/Music-for-Plants

<img src="images/references/concerto-para-plantas.png">

**TEA CEREMONY**
*Tom Sachs*

The exhibition centers on an immersive environment representing Sachs’ distinctive reworking of chanoyu, or traditional Japanese tea ceremony—including the myriad elements essential to that intensely ritualistic universe.

https://www.noguchi.org/museum/exhibitions/view/tom-sachs-tea-ceremony/

<img src="images/references/tea-ceremony.png">

**CONVERSATIONAL IMPLANT**
*Becoming*

By interacting with Conversational Implant chatbot, people take part in a semi-autonomous caring system that is centered around the plant. Humans, artificial intelligence, hardware and software become one cyborg, blurring the limits between what is considered nature and what is considered technology. 

https://becoming.network/ci.html

<img src="images/references/conversational-implant.png">

## Parts

The artifact is divided into three main parts. The electronics, which are a *work in progress*, the main case, where the electronics are enclosed and will serve to be able to play in the future when the electronics are fully defined, the base, that has a double function, hold the electronics in place (the first iteration is plain, but will be modified once the electronics are defines) and create an anchor point for a belt, to attach the device to the actor you want to give voice to.

### Electronics

The basic idea is to have a speaker, connected to an amplifier, that receives the signal from a microphone (or more than one). This will be attached to the element you want to give voice to and will create a staging context to listen to it. The signal from the microphone will be divided so we can use the ESP32 board to look for other ways to represent the voice. Here a small diagram of the concept.

<img src="images/electronics/electronics-diagram.png">

<img src="images/electronics/electronic-parts.png">

### Case

As the electronics haven't been clearly defined yet, we decided to create a case that would allow us some flexibility in the future. We started thinking of 3D printing the case and did a simple cardboard model to see the required dimensions for the electronics we had.

<img src="images/case/carboard-prototype.png">

In order to get a more intense experience in the CNC and looking for a more aesthetic solution, we reframed the idea and decided to work with a block of wood, that we would mill. The case had to be deep enough to enclosure all the electronics as well as have a way to externalise sound, as will install a speaker in the future.

<img src="images/case/cad-1.png">

<img src="images/case/cad-2.png">

#### CNC

The final model was quite a complex operation, as it required properly fixing the scrap block we were working on, a change of mill end, and fliping the block to mill it on both sides. Fixing the block was a very important aspect of the operation as it required to be properly aligned when we did the flipping operation.

<img src="images/case/cnc-1.png">


Four different operations were defined (you can find the files in /case/cnc):

- s1-1-6mmupholes.nc

	The first operation was to drill the holes for the speaker. In order to do that we used an up-cut mill end of 6mm. Using a down-cut, and because it is drilling without coming out from the other side, would have lead to an accumulation of sawdust in the bottom of the hole that could have ended in fire because of the friction of the rotating mill.

	<img src="gif/case/cnc-1.gif">


- s1-2-6mmdownpocketcut.nc

	The second operation required changing the drill. We secured a 6mm down-cut drill, as this time we were cutting the exterior part of the case. We were cut only half the depth, as the whole box was too deep for the drills available in the lab.

	<img src="images/case/cnc-2.png">


- s2-1-6mmdownpocket.nc

	After the two operations were already done, we flipped the block two continue with the other two. The drill wasn't changed, 6mm down-cut. The first operation of this second part was doing the pocketing of the case.

	<img src="gif/case/cnc-2.gif">


	*We found a surprise inside the block!*

	<img src="images/case/cnc-3.png">


- s2-2-6mmdownexteriorfinal.nc

	The final operation was to do the other half of the exterior cut. To do so, we secured the block to the CNC base from the holes of the speaker, which now were visible from the other side once the pocketing was completed.

	<img src="images/case/cnc-4.png">

	<img src="gif/case/cnc-3.gif">

#### Finishing

The final result was very satisfying, although it had a small lip in the outside part because the fliping operation wasn't perfectly aligned, so we sanded the outer part to get a nicer result.

<img src="gif/case/sanding.gif">

We gave it a final layer of varnish to finish it and make it work on the exterior.

<img src="gif/case/varnish.gif">


### Base

The case has a 3d printed base. Here the electronics will be secured. It has four holes to be able to screw it and unscrew it from the case, and have two anchor points to insert a belt and fix the device. You can find the STL file in /base.

#### 3d printing

The base was printed in PLA, with a 0.6 mm nozzle. As it was a plain base, the final width of the layers wasn't very important, so 0.6 mm was a good option. Also, this width allows us to have a much stronger final piece, as well as printing it very fast.

<img src="images/base/slicer.png">

<img src="gif/base/3d-print-1.gif">

<img src="gif/base/3d-print-2.gif">

<img src="images/base/3d-printing.png">

<img src="images/base/final.png">

## Final

We are absolutely satisfied with the final result. The varnish gives it a very interesting final touch and the combination of the wood with the white base is great.

<img src="images/final/final-1.png">

<img src="images/final/final-2.png">

<img src="images/final/final-3.png">

## Future

For the moment it is a base to build upon. We have the necessary electronic parts to start experimenting in the next challenge, and the material of the case will allow us to modify it in order to adapt it to different elements we would like to add, like wires or LEDs. The base for the moment is plain, but once everything is fully defined, we would modify it to secure the electronics inside it. The idea, as explained before will be to install a microphone and speaker, as well as the ESP32 to allow us to play with the microphone signal with Arduino and explore different outputs such as light or a display.